package grails.northwindmongodb

import grails.converters.JSON

class PersonController {

    def index() {
        render "index"
    }

    def add() {
        def obj = new Person(id: '123')
        obj.setLastName("Almario")
        obj.setFirstName("Jose")
//        println obj.getProperties()
        try {
            obj.save(flush:true)
        } catch (Exception e) {
            println e.message
        }

        render 'test'
    }

    def list() {
        def obj = Person.list()

        render obj as JSON
    }
}
