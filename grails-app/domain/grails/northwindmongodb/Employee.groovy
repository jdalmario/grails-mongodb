package grails.northwindmongodb

import org.bson.types.ObjectId
import grails.persistence.Entity

@Entity
class Employee {
    static mapWith = "mongo"

    ObjectId id
    String firstName
    String lastName
    static mapping = {
        collection "employees"
        database "Northwind"

        firstName attr:"FirstName"
        lastName attr:"LastName"

        version false
    }
}
